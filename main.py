"""
A very basic microscope controller that uses pigamepad and a SNES controller
"""

from __future__ import print_function, division

from pigamepad import SnesPad, ButtonPress, ButtonRelease, AxisUpdate
import numpy as np
from queue import Queue
import requests
import time
import sys
import glob

BASE_URI = "http://localhost:5000/api/v2"

class RestartableException(Exception):
    """Exception class that allows the application to just restart itself"""
    def __init__(self, *args, **kwargs):
        Exception.__init__(self, *args, **kwargs)

def move_microscope(move):
    """Send a request to move the microscope (shouldn't return until the move is done)"""
    payload = {'absolute':False}
    for i, k in enumerate(['x', 'y', 'z']):
        payload[k] = int(move[i])
    ret = requests.post(BASE_URI + "/actions/stage/move", json=payload)
    ret.raise_for_status()
    return ret

def do_autofocus():
    ret = requests.post(BASE_URI + "/extensions/org.openflexure.autofocus/fast_autofocus")
    ret.raise_for_status()
    return ret

def capture_image():
    ret = requests.post(BASE_URI + "/actions/camera/capture", json={"bayer":False})
    ret.raise_for_status()
    return ret

def start_stop_preview(running):
    if running:
        command = 'stop'
    else:
        command = 'start'
    ret = requests.post(BASE_URI + "/actions/camera/preview/"+command+"/")
    ret.raise_for_status()
    return ret

def direction_from_snespad(pad):
    """Determine what direction to move in, based on which buttons are pressed."""
    try:
        x = np.sign(pad.axes['x-ax']) * -1
        y = np.sign(pad.axes['y-ax'])
        if pad.buttons['Left Bumper']:
            z = 1
        elif pad.buttons['Right Bumper']:
            z = -1
        else:
            z = 0
        return np.array([x, y, z], dtype=int)
    except:
        return np.zeros(3, dtype=int)

def move_microscope_with_joypad(pad, events,fast=False):
    """Move the stage in the direction indicated by the controller, until it stops"""
    i = 0
    step = [10,10,5]
    delay = 0.05
    if fast:
        step_changes = {1:100, 5:200, 10:500, 15:1000}
    else:
        step_changes = {1:[20,20,10], 5:50, 10:100, 15:200}
    delay_changes = {1:0., 4:0}
    direction = direction_from_snespad(pad)
    while np.any(direction_from_snespad(pad)):
        disp = direction_from_snespad(pad) * step
        print(disp)
        time.sleep(delay)
        move_microscope(disp)
        i += 1
        if i in step_changes: # gradually make longer moves
            step = step_changes[i]
        if i in delay_changes: # gradually make longer moves
            delay = delay_changes[i]
        #time.sleep(0.001*step) # this is about how long a move takes
        while not events.empty():
            events.get() # empty the queue of events, nothing happens while we move!

def main():
    running = False
    print("OpenFlexure Microscope SNESClient")
    print("Waiting for gamepad...")
    events = Queue()
    pad = None
    while not pad:
        pad_paths = glob.glob("/dev/input/js*")
        if len(pad_paths) > 0:
            # TODO: Allow multiple pads?
            pad = SnesPad(pad_paths[0], events.put)
        else:
            time.sleep(5)
    print("Use D pad to move in XY, bumper buttons for Z")
    print("Blue/X will autofocus")
    print("Red/A will capture image")
    print("Yellow/B will enter/exit fast mode")

    fast=False
    try:
        while pad.thread_alive:
            e = events.get()
            if e.name =="exception":
                print("Received an exception event")
                raise RestartableException(e.value)
            if np.any(direction_from_snespad(pad)):
                # If the SNESPad is now telling us to move, process that
                # until the moves stop
                move_microscope_with_joypad(pad, events,fast)
            elif e.name == "X" and isinstance(e, ButtonRelease):
                print("Running autofocus")
                do_autofocus()
            elif e.name == "A" and isinstance(e, ButtonRelease):
                print("Capturing image")
                capture_image()
            elif e.name == "B" and isinstance(e, ButtonRelease):
                fast = not fast
                if fast:
                        mode = "fast"
                else:
                        mode = "standard"
                print(f"Movement mode is now {mode}")
            elif e.name == "start" and isinstance(e, ButtonRelease):
                start_stop_preview(running)
                running = not running
    except KeyboardInterrupt:
        print("Got Ctrl+C, stopping")



if __name__ == '__main__':
    # Infinitely run unless Ctrl+C
    while True:
        try:
            main()
        except RestartableException:
            pass
        else:
            break
