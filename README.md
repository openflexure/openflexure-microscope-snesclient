# openflexure-microscope-snesclient

A hacky little script to control the OpenFlexure Microscope with a SNES controller, using Julian Stirling's pigamepad library.


### Install

* `sudo apt install libpython3-dev`
* `git clone https://gitlab.com/openflexure/openflexure-microscope-snesclient.git`
* `cd openflexure-microscope-snesclient`
* `pip3 install wheel`
* `pip3 install -r requirements.txt`
* `python3 main.py`

### Install as a service

To have the script run on boot, create a systemd service looking like:

```
[Unit]
Description=Python script to control the OpenFlexure Microscope with a gamepad
After=network.target

[Service]
User=pi
Group=pi
WorkingDirectory=/home/pi/openflexure-microscope-snesclient/
ExecStart=python3 /home/pi/openflexure-microscope-snesclient/main.py

[Install]
WantedBy=multi-user.target
```

If you install the application in a virtual environment, update the services Environment, WorkingDirectory, and ExecStart paths accordingly.
